import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css';

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom'
import { Route, Routes } from 'react-router-dom'

import { useState } from 'react';
import { UserProvider } from './UserContext';

import AppNavBar from './components/AppNavBar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import AdminDashboard from './pages/AdminDashboard';
import Products from './pages/Products';
import ProductView from './pages/ProductView';


function App() {
  const [user, setUser] = useState({
    id: localStorage.getItem('id')
  })
  const unsetUser = () => {
    localStorage.clear();
  }
  return (
    <UserProvider value = {{user, setUser, unsetUser }}>
      <Router>
        <Container fluid>
          <AppNavBar />
          <Routes>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/admin" element={<AdminDashboard />} />
            <Route exact path="/products" element={<Products />} />
            <Route exact path="/products/:productId" element={<ProductView />} />
            <Route exact path="/logout" element={<Logout />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;

